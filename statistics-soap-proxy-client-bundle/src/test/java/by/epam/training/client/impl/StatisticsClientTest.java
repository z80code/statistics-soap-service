package by.epam.training.client.impl;

import net.webservicex.GetStatisticsResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StatisticsClientTest {

	private StatisticsClient client;

	@BeforeEach
	void init(){
		client = new StatisticsClient();
	}

	/**
	 * Using Parameterized Test of Junit5.
	 * Test a right logic of class StatisticsClient
	 * with different inner data.
	 */
	@ParameterizedTest
	@DisplayName("test the getStatistics method")
	@CsvSource(value = {"3.0,5.0", "1.0,2.0", "5.0,15.0", "-3.0,7.0", "-3.0,-5.0", "-13.0,-40.0"})
	void getStatistics(double first, double second) throws Exception {

		List<Double> collection = Arrays.asList(first, second);

		GetStatisticsResponse response = client.getStatistics(collection);
		double expected = (first + second) / 2;
		assertEquals(expected, response.getAverage());
	}

	/**
	 * Test stability of returned values with RepeatedTest of Junit5
	 */
	@DisplayName("check the result values 10 times")
	@RepeatedTest(10)
	void getStatisticsRepeated() throws Exception {

		List<Double> collection = Arrays.asList(3.0, 5.0);

		StatisticsClient client = new StatisticsClient();

		GetStatisticsResponse response = client.getStatistics(collection);

		double expected = (3.0 + 5.0) / 2;

		assertEquals(expected, response.getAverage());
	}
}
