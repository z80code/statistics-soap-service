package by.epam.training.client;

import net.webservicex.GetStatisticsResponse;

import java.util.Collection;

/**
 * Interface of service for the client`s class.
 *
 * @param <T> A type of values.
 */
public interface IStatisticsClient<T> {
	/**
	 * A method of service to get statistics the client`s class.
	 *
	 * @param values Collection of values.
	 * @return A statistics response contains different statistics data.
	 */
	GetStatisticsResponse getStatistics(Collection<T> values);
}
