package by.epam.training.client.impl;

import by.epam.training.client.IStatisticsClient;
import net.webservicex.*;

import javax.xml.ws.Holder;
import java.util.Collection;

/**
 * A class implement`s IStatisticsClient<T> interface for the GetStatistics service
 * by url "http://www.webservicex.net/statistics.asmx".
 */

public class StatisticsClient implements IStatisticsClient<Double> {

	private final static ObjectFactory factory =new ObjectFactory();;
	private final static Statistics service=  new Statistics();
	private final StatisticsSoap clientSoap= service.getStatisticsSoap();

	/**
	 * A method of service to get statistics the client`s class.
	 *
	 * @param values Collection of values.
	 * @return A statistics response contains different statistics data.
	 */
	@Override
	public GetStatisticsResponse getStatistics(Collection<Double> values) {

		ArrayOfDouble arrayOfDouble = factory.createArrayOfDouble();
		arrayOfDouble.getDouble().addAll(values);
		return getStatisticsResponse(arrayOfDouble);
	}

	private GetStatisticsResponse getStatisticsResponse(ArrayOfDouble arrayOfDouble) {

		Holder<Double> sums = new Holder<>();
		Holder<Double> average = new Holder<>();
		Holder<Double> standardDeviation = new Holder<>();
		Holder<Double> skewness = new Holder<>();
		Holder<Double> kurtosis = new Holder<>();

		clientSoap.getStatistics(
				arrayOfDouble,
				sums,
				average,
				standardDeviation,
				skewness,
				kurtosis);

		GetStatisticsResponse response = factory.createGetStatisticsResponse();
		response.setAverage(average.value);
		response.setSums(sums.value);
		response.setStandardDeviation(standardDeviation.value);
		response.setSkewness(skewness.value);
		response.setKurtosis(kurtosis.value);
		return response;
	}
}
