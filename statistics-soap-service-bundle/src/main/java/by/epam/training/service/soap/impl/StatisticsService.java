package by.epam.training.service.soap.impl;

import by.epam.training.client.IStatisticsClient;
import by.epam.training.logic.IChecker;
import by.epam.training.service.soap.IStatisticsService;
import net.webservicex.GetStatisticsResponse;

import javax.jws.WebService;
import java.util.List;

/**
 * It`s StatisticsService class implements IStatisticsService.
 * This one have two methods:
 * 'testService()'
 * and
 * 'getAverage(List<Double> values)'
 */
@WebService(
		endpointInterface = "by.epam.training.service.soap.IStatisticsService",
		serviceName = "StatisticsService")
public class StatisticsService implements IStatisticsService {

	private IChecker<Double> checker;

	private IStatisticsClient<Double> statisticsClient;

	public StatisticsService() {
	}

	public StatisticsService(IChecker<Double> checker, IStatisticsClient<Double> statisticsClient) {
		this.checker = checker;
		this.statisticsClient = statisticsClient;
	}

	public IStatisticsClient getStatisticsClient() {
		return statisticsClient;
	}

	public void setStatisticsClient(IStatisticsClient<Double> statisticsClient) {
		this.statisticsClient = statisticsClient;
	}

	public IChecker<Double> getChecker() {
		return checker;
	}

	public void setChecker(IChecker<Double> checker) {
		this.checker = checker;
	}

	/**
	 * A Test method for service check.
	 *
	 * @return a string witch always contents "Success!" value.
	 */
	@Override
	public String testService() {
		return "Success!";
	}

	/**
	 * A Method for receiving an average value of List.
	 *
	 * @param values List<Double> values.
	 * @return An average value.
	 */

	@Override
	public Double getAverage(List<Double> values) {
		GetStatisticsResponse response = statisticsClient.getStatistics(values);
		double result = response.getAverage();
		if (checker.check(response.getSums())) {
			throw new RuntimeException("Sum of all numbers can`t be lower then 0.");
		}
		return result;
	}
}
