package by.epam.training.logic.impl;

import by.epam.training.logic.IChecker;

public class DoubleValueChecker implements IChecker<Double> {

	@Override
	public boolean check(Double value) {
		return value < 0;
	}
}

