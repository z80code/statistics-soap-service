package by.epam.training.exceptions;

import javax.xml.ws.WebFault;

@WebFault
class ValueLowerThenZeroException extends IllegalArgumentException {

	public ValueLowerThenZeroException() {
	}

	public ValueLowerThenZeroException(String s) {
		super(s);
	}

	public ValueLowerThenZeroException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValueLowerThenZeroException(Throwable cause) {
		super(cause);
	}
}
