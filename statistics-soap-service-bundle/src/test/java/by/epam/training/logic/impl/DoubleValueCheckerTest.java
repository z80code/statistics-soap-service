package by.epam.training.logic.impl;

import by.epam.training.logic.IChecker;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

class DoubleValueCheckerTest {

	private IChecker<Double> checker;

	@BeforeEach
	void init() {
		checker = new DoubleValueChecker();
	}

	@ParameterizedTest
	@ValueSource(doubles = {0, 3.5, 2.9, 5.5, 6.9, 10, 9, 50.5, 105.8})
	void checkPAssFalse(double value) {
		assertFalse(checker.check(value));
	}

	@Test
	void standardAssertions() {
		assertEquals(false, checker.check(5.2));
	}

	@Test
	void groupedAssertions() {
		// In a grouped assertion all assertions are executed, and any
		// failures will be reported together.
		assertAll("checker",
				() -> assertEquals(false, checker.check(0.5)),
				() -> assertEquals(false, checker.check(1.8))
		);
	}

	@Test
	void groupedAssumptions() {
		// In a grouped assertion all assertions are executed, and any
		// failures will be reported together.
		assertAll("checker",
				() -> assumeFalse(checker.check(0.5)),
				() -> assumeFalse(checker.check(1.8))
		);
	}

	@Test
	void groupedAssumptionsThat() {
		// In a grouped assertion all assertions are executed, and any
		// failures will be reported together.
		assertAll("checker",
				() -> assumingThat(false, () -> checker.check(0.5)),
				() -> assumingThat(false, () -> checker.check(1.8))
		);
	}

	@Test
	void groupedAssumptionsOnFailureIgnoreMethod() {
		// In a grouped assertion all assertions are executed, and any
		// failures will be reported together.
		assertAll("checker",
				() -> assumeFalse(false)
		);
	}

	@ParameterizedTest
	@ValueSource(doubles = {-3.5, -2.9, -5.5, -6.9, -10, -9, -50.5, -105.8})
	void checkPassTrue(double value) {
		assertTrue(checker.check(value));
	}


	@ParameterizedTest
	@ValueSource(doubles = {0, -3.5, -2.9, -5.5, -6.9, -10, -9, -50.5, -105.8})
	void checkAssumePassTrue(double value) {
		assumeTrue(checker.check(value));
	}

}