package by.epam.training.service.soap.impl;

import by.epam.training.client.IStatisticsClient;
import by.epam.training.client.impl.StatisticsClient;
import by.epam.training.logic.IChecker;
import by.epam.training.logic.impl.DoubleValueChecker;
import net.webservicex.GetStatisticsResponse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("A StatisticsService class test")
class StatisticsServiceTest {

	private StatisticsService statisticsService;
	private IChecker<Double> checker;
	private IStatisticsClient<Double> statisticsClient;
	private GetStatisticsResponse response;
	private List<Double> doubleList;

	/**
	 * Some prepare to test
	 */

	/**
	 * Using @BeforeAll to initialize the needed classes and that constructor args.
	 */
	@BeforeAll
	@DisplayName("init the fields for a test of the StatisticsService")
	void initField() {
		System.out.println("Start tests a StatisticsService");
		// create mock objects
		checker = mock(DoubleValueChecker.class);
		statisticsClient = mock(StatisticsClient.class);
		response = mock(GetStatisticsResponse.class);
	}

	/**
	 * Using @AfterAll to print ending of all tests in this class.
	 */
	@AfterAll
	void doneAll() {
		System.out.println("Done tests a StatisticsService");
	}

	/**
	 * Using @BeforeEach to initialize the tested class with the constructor args.
	 * This one initialize the tested class before each method running of this class.
	 */
	@BeforeEach
	@DisplayName("init the tested class")
	void testClassInit() {
		// init the tested class - StatisticsService, with the mock objects dependencies
		statisticsService = new StatisticsService(checker, statisticsClient);
	}

	/**
	 * In additional to the method named "testClassInit"
	 * I set some behavior for the mock objects.
	 */
	@BeforeEach
	@DisplayName("preset some values of a mock objects")
	void setDefaultMockObjectResult() {
		// preset default values for mock objects
		when(checker.check(anyDouble())).thenReturn(false);
		when(statisticsClient.getStatistics(anyCollection())).thenReturn(response);
	}

	/**
	 * And I reset link to the tested class(hardcode).
	 */
	@AfterEach
	@DisplayName("reset all links to any objects")
	void fieldsAfter() {
		statisticsService = null;
	}

	/**
	 * Tests
	 */

	// Asserts methods.
	@Test
	@DisplayName("test a get method of a StatisticsClient")
	void getStatisticsClientNotNull() {
		assertNotNull(statisticsService.getStatisticsClient());
	}

	@Test
	@DisplayName("test a get method of a StatisticsClient")
	void getCheckerNotNull() {
		assertNotNull(statisticsService.getChecker());
	}

	// Assumptions methods.
	@Test
	@DisplayName("test a get method of a StatisticsClient")
	void getStatisticsClientAssumeNotNull() {
		assumeFalse(null==statisticsService.getStatisticsClient());
	}

	@Test
	@DisplayName("test a get method of a StatisticsClient")
	void getCheckerNotAssumeNull() {
		assumeFalse(null!=statisticsService.getChecker());
	}

	@Test
	@DisplayName("test a get method of a StatisticsClient")
	void getStatisticsClient() {
		assertEquals(statisticsClient, statisticsService.getStatisticsClient());
	}

	// Asserts methods.
	@Test
	@DisplayName("test a set method of a StatisticsClient")
	void setStatisticsClient() {
		final IStatisticsClient<Double> statisticsClientReplaced = mock(StatisticsClient.class);
		statisticsService.setStatisticsClient(statisticsClientReplaced);
		assertAll(
				() -> assertNotEquals(statisticsClient, statisticsService.getStatisticsClient()),
				() -> assertEquals(statisticsClientReplaced, statisticsService.getStatisticsClient())
		);
	}

	@Test
	@DisplayName("test a get method of a Checker")
	void getChecker() {
		assertEquals(checker, statisticsService.getChecker());
	}

	@Test
	@DisplayName("test a set method of a StatisticsClient")
	void setChecker() {
		final IChecker<Double> checkerReplaced = mock(DoubleValueChecker.class);
		statisticsService.setChecker(checkerReplaced);
		assertAll(
				() -> assertNotEquals(checker, statisticsService.getChecker()),
				() -> assertEquals(checkerReplaced, statisticsService.getChecker())
		);
	}

	@Test
	@DisplayName("test testService method")
	void testService() {
		assertEquals("Success!", statisticsService.testService());
	}

	// Using a Parameterized test methods and mock.
	@ParameterizedTest
	@DisplayName("test the getAverage method in case Sum more then zero")
	@ValueSource(doubles = {1.0, 2.5, 5.5, 10.8, 25.8})
	void getAverageIfSumMoreThenZero(double d) {
		final double expected = 2.0;
		when(response.getAverage()).thenReturn(expected);
		assertEquals(expected, (double) statisticsService.getAverage(doubleList));
	}

	@ParameterizedTest
	@DisplayName("test test data received from service")
	@CsvSource(value = {"1.0, 2.5", "3.5, 5.5", "10.8, 25.8"})
	void testCorrectDataFromService(final double a, final double b) {
		when(response.getSums()).thenReturn(a + b);
		double expected = (a + b) / 2;
		when(response.getAverage()).thenReturn(expected);

		when(statisticsClient.getStatistics(anyCollection())).thenReturn(response);
		assertEquals(expected, (double) statisticsService.getAverage(doubleList));
	}

	// Using throw an Exception check.
	@Test
	@DisplayName("test the getAverage method in case Sum lower or equals to zero")
	void getAverageThrowsRuntimeExceptionIfSumLowerOrEqualsToZero() {
		when(checker.check(anyDouble())).thenReturn(true);
		assertThrows(RuntimeException.class, () -> statisticsService.getAverage(doubleList));
	}
}